<?php

namespace ErdmannFreunde\CsvIsotopeStock\Backend;

use Contao\BackendModule;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\FilesModel;
use Contao\Input;
use Contao\System;
use ErdmannFreunde\CsvIsotopeStock\Importer\CsvImporter;

class Import extends BackendModule
{
    protected $strTemplate = 'be_import';

    protected function compile()
    {

        $logger = System::getContainer()->get('monolog.logger.contao');

        $importer = new CsvImporter();

        if ('euf_csvisotopestock_import' === Input::post('FORM_SUBMIT')) {

            $filesModel = FilesModel::findByUuid($this->Config->get('csvisotopestock_csvfile'));
            if ($importer->Import($filesModel)) {
                $logger->info(sprintf("%s Zeilen gelesen, %s geupdated", $importer->getCount(), $importer->getUpdated()), [
                    'contao' => new ContaoContext(__METHOD__, ContaoContext::CRON)
                ]);
            } else {
                $logger->error(sprintf($importer->getLastError()), [
                    'contao' => new ContaoContext(__METHOD__, ContaoContext::CRON)
                ]);
            }

        }

        $this->Template->importer = $importer;
    }
}
