<?php

namespace ErdmannFreunde\CsvIsotopeStock\ContaoManager;

use Contao\CalendarBundle\ContaoCalendarBundle;
use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use ErdmannFreunde\CsvIsotopeStock\CsvIsotopeStockBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(CsvIsotopeStockBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
                ->setLoadAfter([ContaoCalendarBundle::class]),
        ];
    }
}
