<?php

namespace ErdmannFreunde\CsvIsotopeStock\Cron;

use Contao\Config;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\FilesModel;
use Contao\System;
use ErdmannFreunde\CsvIsotopeStock\Importer\CsvImporter;

class CronImporter
{

    public function runCron()
    {

        $logger = System::getContainer()->get('monolog.logger.contao');

        $config = Config::getInstance();
        if ($config->get('csvisotopestock_cron_active')) {

            $interval = (int) $config->get('csvisotopestock_cron_interval');
            if ($interval < 1) {
                return;
            }

            $currentMinute = (int) time() / 60;
            if ($currentMinute % $interval !==0) {
                return;
            }

            $importer = new CsvImporter();

            $filesModel = FilesModel::findByUuid($config->get('csvisotopestock_csvfile'));
            if ($importer->Import($filesModel)) {
                $logger->info(sprintf("%s Zeilen gelesen, %s geupdated", $importer->getCount(), $importer->getUpdated()), [
                    'contao' => new ContaoContext(__METHOD__, ContaoContext::CRON)
                ]);
            } else {
                $logger->error(sprintf($importer->getLastError()), [
                    'contao' => new ContaoContext(__METHOD__, ContaoContext::CRON)
                ]);
            }

        }

    }
}
