<?php
namespace ErdmannFreunde\CsvIsotopeStock\Importer;

use Veello\IsotopeStockManagementBundle\Entry\ManualEntry;
use Veello\IsotopeStockManagementBundle\QuantityResolver;
use Veello\IsotopeStockManagementBundle\StockProduct;
use Contao\Environment;
use Contao\FilesModel;
use Exception;
use Isotope\Model\Product;

class CsvImporter {

    private $row = 0;
    private $count = 0;
    private $updated = 0;
    private $lastError = '';

    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->row;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @return string
     */
    public function getLastError(): string
    {
        return $this->lastError;
    }

    public function Import(FilesModel $filesModel) {

        $this->row = 0;
        $this->updated = 0;
        $this->count = 0;
        $this->lastError = '';

        try {
            // Csv einlesen
            if (($handle = fopen(Environment::get('documentRoot') . '/../' . $filesModel->path, "r")) !== FALSE) {
                $header = [];
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                    if ($this->row === 0 ) {
                        $header = $data;
                    } else {
                        $artikelNr = $data[array_search('ArtikelNr', $header)];
                        //$bezeichnung = $data[array_search('Bezeichnung', $header)];
                        $gesamtbestand = (int) $data[array_search('Gesamtbestand', $header)];

                        $product = Product::findOneBy('sku', $artikelNr);

                        if ($product instanceof StockProduct) {
                            // Aktuellen Bestand ermitteln
                            $shopbestand = (int) $product->getStockQuantity();

                            if ($gesamtbestand !== $shopbestand) {
                                // Bestand unterschiedlich -> updaten

                                $quantity = new QuantityResolver($gesamtbestand, QuantityResolver::MODE_SET);
                                $quantity->setCurrentQuantity($shopbestand);

                                $entry = new ManualEntry($product, $quantity);
                                $entry->setNotes('Importiert von CsvIsotopeStock');
                                $entry->save();

                                $this->updated++;

                            }

                        }

                        $this->count++;

                    }

                    $this->row++;

                }
                fclose($handle);
            }
        } catch (Exception $exception) {
            $this->lastError = $exception->getMessage();
            return false;
        }

        return true;
    }

}