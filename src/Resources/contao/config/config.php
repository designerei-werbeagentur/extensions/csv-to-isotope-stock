<?php

use ErdmannFreunde\CsvIsotopeStock\Backend\Import;
use ErdmannFreunde\CsvIsotopeStock\Cron\CronImporter;

$GLOBALS['TL_CRON'] = array(
    'minutely' => array(
        array(CronImporter::class, 'runCron'),
    )
);

array_insert(
    $GLOBALS['BE_MOD']['isotope'],
    2,
    [
        'CsvIsotopeStock' => [
            'callback' => Import::class
        ]
    ]
);