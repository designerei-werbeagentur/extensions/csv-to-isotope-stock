<?php

$GLOBALS['TL_DCA']['tl_settings']['fields']['csvisotopestock_csvfile'] = [
    'label' => $GLOBALS['TL_LANG']['tl_settings']['csvisotopestock_csvfile'],
    'inputType' => 'fileTree',
    'eval' => ['tl_class' => 'w100', 'files' => true, 'filesOnly' => true]
];

$GLOBALS['TL_DCA']['tl_settings']['fields']['csvisotopestock_cron_active'] = [
    'label' => $GLOBALS['TL_LANG']['tl_settings']['csvisotopestock_cron_active'],
    'inputType' => 'checkbox',
    'eval' => ['tl_class' => 'w50'],
    'default' => false
];

$GLOBALS['TL_DCA']['tl_settings']['fields']['csvisotopestock_cron_interval'] = [
    'label' => $GLOBALS['TL_LANG']['tl_settings']['csvisotopestock_cron_interval'],
    'inputType' => 'text',
    'eval' => ['tl_class' => 'w50', 'rgxp' => 'natural'],
    'default' => 60
];

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{csvisotopestock_title},csvisotopestock_csvfile,csvisotopestock_cron_active,csvisotopestock_cron_interval';

